/**
 * Class which do work with numbers
 * @author Alexey*/
public  class Calk {
    /**
     * Get sum (double) of two numbers
     * @param First first number
     * @param Second  second number*/
    public static double Sum (double First, double Second){
        return First + Second;
    }
    /**
     * Get difference (double) of two numbers
     * @param First first number
     * @param Second  second number*/
    public static double Diff(double First, double Second){
        return First - Second;
    }
    /**
     * Get ratio (double) of two numbers
     * @param First first number
     * @param Second  second number*/
    public static double Ratio(double First, double Second){
        return First / Second;
    }
    /**
     * Get exponentiation (double)
     * @param First first number
     * @param Second  second number*/
    public static double Degree (double First, int Second){
        double Answer = 1;
        if (Second > 0)
            for (int i = 0; i < Second; i++){
                Answer = Answer * First;
            }
        if (Second < 0)
            for (int i = 0; i < -1*Second; i++){
                Answer = Answer / First;
            }
        return Answer;
    }
    /**
     * Get multiple (double) of two numbers
     * @param First first number
     * @param Second  second number*/
    public static double Multy(double First, double Second){
        return First * Second;
    }

    public static int ReConv (String First ){
        return Integer.parseInt(First, 2);
    }

    public static String Conv(int First){
        return Integer.toBinaryString(First);
    }
    public static double Ln(double First){
        return Math.log(First);
    }

    public static double Abs(double First){
        return Math.abs(First);
    }
}
