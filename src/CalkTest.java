import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class CalkTest {

    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0, 1, 1",
            "-25, 40, 15",
            "156, 50, 206"})
    void sum(double First, double Second, double expected) {
        assertEquals(expected, Calk.Sum(First, Second));
    }
    @ParameterizedTest(name = "{0} - {1} = {2}")
    @CsvSource({
            "0, 1, -1",
            "-25, -40, 15",
            "156, 56, 100"
    })
    void diff(double First, double Second, double expected) {
        assertEquals(expected, Calk.Diff(First, Second));
    }
    @ParameterizedTest(name = "{0} / {1} = {2}")
    @CsvSource({
            "0, 1, 0",
            "-5, -4, 1.25",
            "100, -5, -20"
    })
    void ratio(double First, double Second, double expected) {
        assertEquals(expected, Calk.Ratio(First, Second));
    }
    @ParameterizedTest(name = "{0} ^ {1} = {2}")
    @CsvSource({
            "0, 3, 0",
            "-5, -1, -0.2",
            "100, 1, 100"
    })
    void degree(double First, int Second, double expected) {
        assertEquals(expected, Calk.Degree(First, Second));
    }
    @ParameterizedTest(name = "{0} * {1} = {2}")
    @CsvSource({
            "0, 1, 0",
            "-5, -4, 20",
            "100, -5, -500"
    })
    void multy(double First, double Second, double expected) {
        assertEquals(expected, Calk.Multy(First, Second));
    }
}