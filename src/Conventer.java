enum Type{
    Sum,
    Diff,
    Multy,
    Ratio,
    Degree,
    Bin,
    ReBin,
    Ln,
    Abs
}
/**
 * Class which convert user`s request into calk command
 * and return answer or exeption message in string format
 * @author Alexey*/
public class Conventer {
    /**
     * @param Values User`s request (two numbers in string format)
     * @param Aim Type of calk`s commands*/
    static public String Convert(String[] Values, Type Aim ){
        try{
            String Answer;
            switch (Aim){
                case Sum:
                    double a = Double.parseDouble(Values[0]);
                    double b = Double.parseDouble(Values[1]);
                    Answer = "Ответ: " + Double.toString(Calk.Sum(a, b));
                    return Answer;
                case Diff:
                    a = Double.parseDouble(Values[0]);
                    b = Double.parseDouble(Values[1]);
                    Answer = "Ответ: " + Double.toString(Calk.Diff(a, b));
                    return Answer;
                case Multy:
                    a = Double.parseDouble(Values[0]);
                    b = Double.parseDouble(Values[1]);
                    Answer = "Ответ: " + Double.toString(Calk.Multy(a, b));
                    return Answer;
                case Ratio:
                    a = Double.parseDouble(Values[0]);
                    b = Double.parseDouble(Values[1]);
                    if (b == 0) {
                        return "Error";
                    }
                    Answer = "Ответ: " + Double.toString(Calk.Ratio(a, b));
                    return Answer;
                case Degree:
                    a = Double.parseDouble(Values[0]);
                    int c= Integer.parseInt(Values[1]);
                    Answer = "Ответ: " + Double.toString(Calk.Degree(a, c));
                    return Answer;
                case Bin:
                    c = Integer.parseInt(Values[0]);
                    Answer = "Ответ: " + Calk.Conv(c);
                    return Answer;
                case ReBin:
                    Answer = "Ответ: " + Calk.ReConv(Values[0]);
                    return Answer;
                case Ln:
                    a = Double.parseDouble(Values[0]);
                    Answer = "Ответ: " + Calk.Ln(a);
                    return Answer;
                case Abs:
                    a = Double.parseDouble(Values[0]);
                    Answer = "Ответ: " + Calk.Abs(a);
                    return Answer;

            }
        }
        catch(Exception e){
            return "Error";
        }
        return "Error";
    }

}
