import java.util.Scanner;
/**
 * Class which read user`s requests and send it to HConventer
 * @author Alexey*/

public class Menu {

    /**
     * Method which print available commands
     */
    static public void Commands(){
        System.out.println("Список команд:");
        System.out.println("\t-1 - Запросить список комманд");
        System.out.println("\t0 - Выход из программы");
        System.out.println("\t1 - Сумма двух чисел");
        System.out.println("\t2 - Разность двух чисел");
        System.out.println("\t3 - Произведение двух чисел");
        System.out.println("\t4 - Частное двух чисел");
        System.out.println("\t5 - Возведение в степень");
        System.out.println("\t6 - Перевод в двоичную систему");
        System.out.println("\t7 - Перевод в двоичную систему");
        System.out.println("\t8 - Натуральный логарифм от числа");
        System.out.println("\t9 - Модуль числа");
    }

    /**
     * Method which read user`s commands
     */
    static public void Start(){
        HConventer hConventer = new HConventer();
        Menu.Commands();
        System.out.println("Введите команду");
        Scanner in = new Scanner(System.in);
        String Command = in.nextLine();
        while(Command != "0"){
            switch (Command){
                case "0":
                    System.out.println("Завершение программы");
                    return;
                case "1":
                    System.out.println("Введите два числа");
                    String tempOne = in.nextLine();
                    String tempTwo = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne, tempTwo}, Type.Sum));
                    break;
                case "2":
                    System.out.println("Введите два числа");
                    tempOne = in.nextLine();
                    tempTwo = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne, tempTwo}, Type.Diff));
                    break;
                case "3":
                    System.out.println("Введите два числа");
                    tempOne = in.nextLine();
                    tempTwo = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne, tempTwo}, Type.Multy));
                    break;
                case "4":
                    System.out.println("Введите два числа");
                    tempOne = in.nextLine();
                    tempTwo = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne, tempTwo}, Type.Ratio));
                    break;
                case "5":
                    System.out.println("Введите два числа");
                    tempOne = in.nextLine();
                    tempTwo = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne, tempTwo}, Type.Degree));
                    break;
                case "-1":
                    Menu.Commands();
                    break;
                case "6":
                    System.out.println("Введите целое число");
                    tempOne = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne}, Type.Bin));
                    break;
                case "7":
                    System.out.println("Введите число в двоичной системе");
                    tempOne = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne}, Type.ReBin));
                    break;
                case "8":
                    System.out.println("Введите целое число");
                    tempOne = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne}, Type.Ln));
                    break;
                case "9":
                    System.out.println("Введите  число");
                    tempOne = in.nextLine();
                    System.out.println(hConventer.HConvert(new String[]{tempOne}, Type.Abs));
                default:
                    System.out.println("Неверная команда");
                    Menu.Commands();
                    break;
            }
            System.out.println("Введите команду");
            Command = in.nextLine();
        }
    }
}