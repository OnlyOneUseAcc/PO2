import java.util.LinkedList;

public class HConventer  {
    private LinkedList<String> History;
    public HConventer (){
        History = new LinkedList<String>();
    }
    public String HConvert (String[] Values, Type Aim){
       if (Values.length == 1){
           History.add(Aim.toString() + Values[0]);
       }
       if (Values.length == 2){
           History.add(Values[0] + Aim.toString() + Values[1]);
       }
       return Conventer.Convert(Values, Aim);
    }
    public void printHistory(){
        for (String Operation : History) {
            System.out.println(Operation);
        }
    }
}
