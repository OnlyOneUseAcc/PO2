import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class ConventerTest {

    @ParameterizedTest(name = "Test of Conventer {2}")
    @CsvSource({
            "1 ,2, Sum, Ответ: 3.0",
            "1, -5, Diff, Ответ: 6.0",
            "25, 3, Multy, Ответ: 75.0",
            "25, 5, Ratio, Ответ: 5.0",
            "2, 2, Degree, Ответ: 4.0"})
    void convert(String First, String Second, Type Aim, String expected) {
        assertEquals(Conventer.Convert(new String[]{First, Second}, Aim), expected);
    }
}